﻿using System;

namespace DelegatesEvents
{
    public class FileArgs : EventArgs
    {
        public string FileName { get; set; }
        public int Count { get; set; }
    }

}