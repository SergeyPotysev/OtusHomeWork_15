﻿using System;
using System.Collections.Generic;
using System.IO;

namespace DelegatesEvents
{
    static class Program
    {
        private static MyFiles _files;
        static void Main(string[] args)
        {
            _files = new MyFiles(100, @"D:\");
            // register with an event
            _files.FileFound += FileFoundAction;
            _files.StartSearch();
            Func<FileInfo, float> getParametr = B2MB;
            var largestFile = GetMax(_files.FileList, getParametr);
            Console.WriteLine($"\nНаибольший размер у файла: {largestFile.FullName}");
            Console.ReadLine();
        }


        // event handler
        public static void FileFoundAction(object sender, FileArgs e)
        {
            Console.WriteLine($"{e.Count,-3} Найден файл {e.FileName}");
            _files.FileList.Add(new FileInfo(e.FileName));
        }


        /// <summary>
        /// Обобщённая функция расширения, находящуя и возвращающая максимальный элемент коллекции.
        /// </summary>
        public static T GetMax<T>(this IEnumerable<T> e, Func<T, float> getParametr) where T : class
        {
            float length, maxLength = 0;
            T largestFile = default;
            foreach (T item in e)
            {
                length = getParametr(item);
                if (length > maxLength)
                {
                    maxLength = length;
                    largestFile = item;
                }
            }
            return largestFile;
        }


        /// <summary>
        /// Возвращает размер файла в мегабайтах.
        /// </summary>
        private static float B2MB(FileInfo file)
        {
            return Convert.ToSingle((file.Length / 1024f) / 1024f);
        }

    }
}