﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace DelegatesEvents
{
    class MyFiles
    {
        // declaring an event using built-in EventHandler
        public event EventHandler<FileArgs> FileFound;
        private int _maxCount;
        private string _drive;
        public List<FileInfo> FileList { get; set; }

        public MyFiles(int maxCount, string drive)
        {
            _maxCount = maxCount;
            _drive = drive;
            FileList = new List<FileInfo>();
        }


        /// <summary>
        /// Наполнение рабочей коллекции информацией о файлах. 
        /// </summary>
        public void StartSearch()
        {
            int n = 0;
            var data = new FileArgs();
            Console.Clear();
            Console.WriteLine($"Количество итераций поиска файлов: {_maxCount}");
            Console.WriteLine("Нажмите ESC для завершения работы ...\n");
            foreach (string dirEntry in Directory.GetDirectories(@_drive))
            {
                foreach (string fileEntry in Directory.GetFiles(@dirEntry))
                {
                    Thread.Sleep(250);
                    data.FileName = fileEntry;
                    data.Count = n + 1;
                    OnFileFound(data);
                    while (Console.KeyAvailable)
                    {
                        // Выход из цикла по кнопке Esc
                        if (Console.ReadKey(true).Key == ConsoleKey.Escape)
                        {
                            n = _maxCount;
                        }
                    }
                    if (++n >= _maxCount)
                    {
                        return;
                    }
                }
            }
        }


        /// <summary>
        /// Запуск обработчика события.
        /// </summary>
        protected virtual void OnFileFound(FileArgs e)
        {
            FileFound?.Invoke(this, e);
        }

    }

}